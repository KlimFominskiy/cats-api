import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const HttpClient = Client.getInstance();
const cats: CatMinInfo[] = [
    { name: 'Кфа-первый', description: '', gender: 'male' },
    { name: 'Кфа-второй', description: '', gender: 'female' },
];
const invalidCatId = 'A';
const validGenderRegularExpression = /^(male|female|unisex)$/;
const catDescription = "Это описание имени для тестирования работы метода API.";

let catIds = new Array<Number>();

describe('API core',() => {
    beforeAll(async() => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            for (let i : number = 0; i < cats.length; i++) {
                if ((add_cat_response.body as CatsList).cats[0].id) {
                    catIds.push((add_cat_response.body as CatsList).cats[i].id);
                } else throw new Error('Не удалось получить id тестового котика!');
            }
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });
    
    afterAll(async() => {
        for (let i : number = 0; i < cats.length; i++) {
                await HttpClient.delete(`core/cats/${catIds[i]}/remove`, {
                responseType: 'json',
            });
        }
    });

    it('Найти котика по корректному id.', async() => {
        // Act.
        const response = await HttpClient.get(`core/cats/get-by-id/?id=${catIds[0]}`, {
            responseType: 'json'
        });

        // Assert.
        expect(response.statusCode).toEqual(200);
        expect(response.body).toMatchObject({
            cat: {
                id: catIds[0],
                ...cats[0],
                tags: null,
                likes: 0,
                dislikes: 0,
            }
        });
    });

    it('Найти котика по некорректному id.', async() => {
        // Act ans assert.
        await expect(
            HttpClient.get(`core/cats/get-by-id/?id=${invalidCatId}`, {
            responseType: 'json',
            })
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });

    it('Добавить описание котику.', async() => {
        // Act.
        const response = await HttpClient.post('core/cats/save-description', {
            responseType: 'json',
            json: {
                catId: catIds[1],
                catDescription: catDescription
            },
        });

        // Assert.
        expect(response.statusCode).toEqual(200);
        expect(response.body).toMatchObject({
            id: catIds[1],
            name: cats[1].name,
            description: catDescription,
            tags: null,
            gender: cats[1].gender,
            likes: 0,
            dislikes: 0,

        });
    });
    
    it('Получить список котов, распределённых по группам.', async() => {
        // Act.
        const response = await HttpClient.get('core/cats/allByLetter', {
            responseType: 'json'
        })

        // Assert.
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            groups: expect.arrayContaining([
                expect.objectContaining({
                    title: expect.any(String),
                    cats: expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            description: expect.any(String),
                            tags: null,
                            gender: expect.stringMatching(validGenderRegularExpression),
                            likes: expect.any(Number),
                            dislikes: expect.any(Number),
                       }),
                    ]),
                    count_in_group: expect.any(Number),
                    count_by_letter: expect.any(Number),
                }),
            ]),
            count_output: expect.any(Number),
            count_all: expect.any(Number),
        });
    });
});